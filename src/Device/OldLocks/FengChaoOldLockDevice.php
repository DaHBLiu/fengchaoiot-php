<?php
/**
 * Created by LHB
 * User: LHB
 * Date: 2019/5/15
 * Time: 10:08
 * Email:804433577@qq.com
 */

namespace FengChaoIOT\Device\OldLocks;


use FengChaoIOT\IOT;
use FengChaoIOT\Request\IOTRequest;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class FengChaoOldLockDevice
{
    use IOT, IOTRequest;
    private $appKey;
    private $appSecret;
    private $timestamp;
    private $url = 'http://lease.yunsuowang.com';

    /**
     * 构造函数
     * FengChaoOldLockDevice constructor.
     * @param $appKey
     * @param $appSecret
     */
    function __construct($appKey, $appSecret)
    {
        $this->appKey = $appKey;
        $this->appSecret = $appSecret;
        $this->timestamp = time();
    }

    /**
     * sign
     * @param array $requestParams
     * @return string
     */
    public function makeSign(array $requestParams)
    {
        $requestParams['timestamp'] = $this->timestamp;
        ksort($requestParams);
        $str = $this->appKey;
        foreach ($requestParams as $key => $param) {
            $str .= $key . '=' . $param;
        }
        $str .= $this->appSecret;
        $sha_str = sha1($str);
        $sign = $this->String2Hex($sha_str);
        return $sign;
    }

    /**
     * 添加新锁
     * @param $hotelId
     * @param $lockId
     * @param $roomId
     * @param $remark
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function addNewLocks($hotelId, $lockId, $roomId, $remark)
    {
        $requestParams = [
            'hotelId' => $hotelId,
            'lockId' => $lockId,
            'roomId' => $roomId,
            'remark' => $remark
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['form_params'] = $requestParams;
        $response = $this->request($this->url, '/api/locks', $request);
        return $response;
    }


    /**
     * 修改门锁信息
     * @param $hotelId
     * @param $lockId
     * @param $roomId
     * @param $remark
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function updateLocks($hotelId, $lockId, $roomId, $remark)
    {
        $requestParams = [
            'hotelId' => $hotelId,
            'roomId' => $roomId,
            'remark' => $remark
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['body'] = http_build_query($requestParams);
        $response = $this->request($this->url, '/api/locks/' . $lockId, $request, 'PUT');
        return $response;
    }

    /**
     * 删除门锁
     * @param $lockId
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function deleteLocks($lockId)
    {
        $requestParams = [
            'lockId' => $lockId,
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['body'] = http_build_query($requestParams);
        $response = $this->request($this->url, '/api/locks/' . $lockId, $request, 'DELETE');
        return $response;
    }

    /**
     * 获取门锁信息
     * @param string $hotelId
     * @param string $roomId
     * @param int $page
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function getLocksList($hotelId = '', $roomId = '', $page = 1)
    {
        $requestParams = [
            'page' => $page
        ];
        if ($hotelId) {
            $requestParams['hotelId'] = $hotelId;
        }
        if ($roomId) {
            $requestParams['roomId'] = $roomId;
        }

        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['query'] = $requestParams;
        $response = $this->request($this->url, '/api/locks', $request, 'GET');
        return $response;
    }


    /**
     * 向已经安装好了的门锁中添加用户使用权限
     * @param $lockId
     * @param $userId
     * @param $hotelId
     * @param $startTime string 普通用户添加时必要
     * @param $endTime string 普通用户添加时必要
     * @param string $type [TENANT,MANAGER]
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function addUserPermissions($lockId, $userId, $hotelId, $startTime, $endTime, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type
        ];
        if ($type == 'TENANT') {
            $requestParams['startTime'] = $startTime;
            $requestParams['endTime'] = $endTime;
        }
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => $this->appKey
        ];
        $request['body'] = json_encode($requestParams);
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/permissions', $request);
        return $response;
    }

    /**
     * 删除用户权限
     * @param $lockId
     * @param $userId
     * @param $hotelId
     * @param string $type @value in [TENANT,MANAGER]
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException]
     */
    public function deleteUserPermissions($lockId, $userId, $hotelId, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey,
        ];
        $request['true '] = true;
        $request['http_errors'] = true;
        $request['query'] = $requestParams;
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/permissions', $request, 'DELETE');
        return $response;
    }

    /**
     * 老板子同步时间
     * @param $lockId
     * @param $userId
     * @param $hotelId
     * @param string $type
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function syncTime($lockId, $userId, $hotelId, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['query'] = $requestParams;
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/packets/time', $request, 'GET');
        return $response;
    }

    /**
     * 获取开锁指令
     * @param $lockId
     * @param $userId
     * @param $hotelId
     * @param string $type
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function openDoor($lockId, $userId, $hotelId, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['query'] = $requestParams;
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/packets/opendoor', $request, 'GET');
        return $response;
    }


    /**
     * 获取门锁事件指令
     * @param $lockId
     * @param $userId
     * @param $hotelId
     * @param string $type
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function getEvents($lockId, $userId, $hotelId, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['query'] = $requestParams;
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/packets/event', $request, 'GET');
        return $response;
    }

    /**
     * 解析指令包
     * @param $lockId
     * @param $data
     * @param $userId
     * @param $hotelId
     * @param string $type
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function decodePackets($lockId, $data, $userId, $hotelId, $type = 'TENANT')
    {
        $requestParams = [
            'userId' => $userId,
            'hotelId' => $hotelId,
            'type' => $type,
            'data' => $data
        ];
        $sign = $this->makeSign($requestParams);
        $requestParams['timestamp'] = $this->timestamp;
        $requestParams['sign'] = $sign;
        $request['headers'] = [
            'Authorization' => $this->appKey
        ];
        $request['form_params'] = $requestParams;
        $response = $this->request($this->url, '/api/locks/' . $lockId . '/packets', $request, 'POST');
        return $response;
    }
}