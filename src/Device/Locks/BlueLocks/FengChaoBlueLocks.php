<?php
/**
 * Created by LHB
 * User: LHB
 * Date: 2019/4/15
 * Time: 15:17
 * Email:804433577@qq.com
 */

namespace FengChaoIOT\Device\Locks\BlueLocks;


use GuzzleHttp\Exception\GuzzleException;
use FengChaoIOT\Device\Locks\FengChaoLocks;
use FengChaoIOT\IOT;
use Psr\Http\Message\ResponseInterface;

class FengChaoBlueLocks extends FengChaoLocks
{
    use IOT;

    function __construct($deviceID, $token)
    {
        parent::__construct($deviceID, $token);
    }

    /**
     * 添加密码
     * @param $type
     * @param $password
     * @param $startTime  YYYY-MM-DD HH:mm:ss
     * @param $endTime  YYYY-MM-DD HH:mm:ss
     * @param $userId
     * @param array $weeks
     * @return mixed
     * @throws GuzzleException
     */
    public function addPasswords($type, $password, $startTime, $endTime, $userId, array $weeks)
    {
        //$type PLAIN普通密码 CYCLE周期密码 MANAGER管理密码
        $this->uri = '/ble-locks/' . $this->deviceId . '/passwords';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [];
        $bodyArr['type'] = $type;
        $bodyArr['password'] = $password;
        $bodyArr['startTime'] = $startTime;
        $bodyArr['endTime'] = $endTime;
        $bodyArr['userId'] = $userId ? $userId : 0;
        $bodyArr['weeks'] = $weeks ? $weeks : [];
        $requestParams['body'] = json_encode($bodyArr);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 添加卡片
     * @param $type
     * @param $cardNumber
     * @param $startTime
     * @param $endTime
     * @param $userId
     * @param array $weeks
     * @return mixed
     * @throws GuzzleException
     */
    public function addCards($type, $cardNumber, $startTime, $endTime, $userId, array $weeks)
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/cards';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [];
        $bodyArr['type'] = $type;
        $bodyArr['startTime'] = $startTime;
        $bodyArr['endTime'] = $endTime;
        $bodyArr['userId'] = $userId ? $userId : 0;
        $bodyArr['weeks'] = $weeks ? $weeks : [];
        $requestParams['body'] = json_encode($bodyArr);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 本地添加指纹
     * 通过接口获取门锁录制指纹指令数据包，然后通过手机蓝牙将该数据包传输到门锁，进行本地指纹采集操作。
     * @param $startTime
     * @param $endTime
     * @param int $userId
     * @param array $fingerprint
     * @return mixed
     * @throws GuzzleException
     */
    public function addFingers($startTime, $endTime, $userId = 0, array $fingerprint = [])
    {
        //蓝牙锁 指纹 和 NB 不一样
        $this->uri = '/ble-locks/' . $this->deviceId . '/fingers';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [];
        $bodyArr['startTime'] = $startTime;
        $bodyArr['endTime'] = $endTime;
        $bodyArr['userId'] = $userId ? $userId : 0;
        $requestParams['body'] = json_encode($bodyArr);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 批量操作门锁权限
     * @param $operate
     * @param string $userId
     * @param string $authId
     * @return mixed
     * @throws GuzzleException
     */
    public function authorities($operate, $userId = '', $authId = '')
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/authorities';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [];
        if ($userId || $userId = 0) {
            $bodyArr['userId'] = $userId ? $userId : 0;
        }
        if ($authId) {
            $bodyArr['authId'] = $authId;
        }
        $requestParams['body'] = json_encode($bodyArr);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }


    /**
     * 提交蓝牙指令执行结果
     * @param $commandId
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function uploadBlueRes($commandId)
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/commands/' . $commandId;
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 蓝牙开门
     * @param int $userId
     * @param  $optType
     * @param  $delayTime
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function openLockDoor($userId = 0, $optType = '', $delayTime = '')
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/key';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [];
        if ($userId || $userId === 0) {
            $bodyArr['userId'] = $userId;
        }
        if ($optType !== '') {
            $bodyArr['optType'] = $optType;
        }
        if ($delayTime !== '') {
            $bodyArr['delayTime'] = $delayTime;
        }
        $requestParams['query'] = $bodyArr;
        return $this->request($this->baseUri, $this->uri, $requestParams, 'GET');
    }

    /**
     * 蓝牙门锁设置
     * @param array $arr
     * @param string $brdName
     * @param int $brdInterval
     * @param int $brdLevel
     * @param string $switchMac
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function setting($arr = [], $brdName = '', int $brdInterval = 0, int $brdLevel = 100, $switchMac = '')
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/setting';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $bodyArr = [
            'brdInterval' => $brdInterval,
            'brdLevel' => $brdLevel
        ];
        if ($brdName) {
            $bodyArr['brdName'] = $brdName;
        }
        if ($switchMac) {
            $bodyArr['switchMac'] = $switchMac;
        }
        if ($arr) {
            $bodyArr = array_merge($bodyArr, $arr);
        }
        $requestParams['body'] = json_encode($bodyArr);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 同步蓝牙锁的时间
     * @throws GuzzleException
     */
    public function syncTime()
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/time';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        return $this->request($this->baseUri, $this->uri, $requestParams, 'GET');
    }

    /**
     *  本地门锁事件同步
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function syncEvents()
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/events';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        return $this->request($this->baseUri, $this->uri, $requestParams, 'GET');
    }

    /**
     *  提交门锁事件
     * @param string $eventData
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function uploadEvents(string $eventData)
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/events';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $requestParams['body'] = json_encode([
            'eventData' => $eventData
        ]);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 获取门锁信息 如 电量
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function getDeviceInfo()
    {
        $this->uri = '/ble-locks/' . $this->deviceId . '/info';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 获取事件同步
     * @param string $type
     * @param string $startTime
     * @param string $endTime
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function getDeviceEvents($type = '', $startTime = '', $endTime = '')
    {
        $bodyArr = [];
        if ($type) {
            $bodyArr['type'] = $type;
        }
        if ($startTime !== '') {
            $bodyArr['startTime'] = $startTime;
        }
        if ($endTime !== '') {
            $bodyArr['endTime'] = $endTime;
        }
        $requestParams['query'] = $bodyArr;
        $this->uri = '/devices/' . $this->deviceId . '/events';
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        return $this->request($this->baseUri, $this->uri, $requestParams, 'GET');
    }
}