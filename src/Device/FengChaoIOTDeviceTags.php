<?php
/**
 * Created by LHB
 * User: LHB
 * Date: 2019/4/15
 * Time: 14:41
 * Email:804433577@qq.com
 */

namespace FengChaoIOT\Device;


use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class FengChaoIOTDeviceTags extends FengChaoIOTDevice
{
    public function __construct($deviceId,$token)
    {
        parent::__construct($token);
        $this->uri = '/devices/' . $deviceId . '/tags';
    }


    /**
     * 修改设备标签
     * @param $tags
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function updateTags($tags)
    {
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $requestParams['body'] = json_encode($tags);
        return $this->request($this->baseUri, $this->uri, $requestParams);
    }

    /**
     * 删除设备标签
     * @param $tags
     * @return array|mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function deleteTags($tags)
    {
        $requestParams['headers'] = array_merge(HONE_COMB_IOT_HEADERS, $this->authorization);
        $requestParams['body'] = json_encode($tags);
        return $this->request($this->baseUri, $this->uri, $requestParams, 'DELETE');
    }
}