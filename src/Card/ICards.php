<?php

/**
 * 门禁卡基础类
 */
namespace FengChaoIOT\Card;


use FengChaoIOT\Request\IOTRequest;

abstract class ICards
{
    use IOTRequest;

    abstract public function make();
}