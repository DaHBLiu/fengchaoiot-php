<?php
/**
 * Created by LHB
 * User: LHB
 * Date: 2019/4/30
 * Time: 10:37
 * Email:804433577@qq.com
 */

namespace FengChaoIOT\Token;

/**
 * token需根据使用框架实现
 * Class TokenManager
 * @package FengChaoIOT\Token
 */
class TokenManager implements Token
{
    public $token;


    public function __construct(object $token)
    {
        $this->token = $token;
    }

    /**
     * @param $string
     * @return mixed
     */
    public function set($string)
    {
        return $this->token->set($string);
    }

    public function get($key = '')
    {
        return $this->token->get($key);
    }

    public function delete()
    {
        return $this->token->delete();
    }

    public function isVoid()
    {
        return $this->token->isVoid();
    }

    public function reload()
    {
        return $this->token->reload();
    }

}